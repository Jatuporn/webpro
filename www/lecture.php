<!--<?php
$txt = "Hello World!";
$number = 10;
echo $txt."<br>";
echo $number;

echo "<h4>This is a simple heading.</h4>";
echo "<h4 style='color:red;'>This is heading with style.</h4>";
$a = 123;
var_dump($a);
echo "<br>";

$colors = array("Red","Green","Blue");
var_dump($colors);
echo "<br>";

$color_codes = array("Red" =>"#ff000",
					"Green"=>"#00ff00",
					"Blue"=>"#0000ff" 
);
var_dump($color_codes);
echo "<br>";
class greeting{
	public $str = "Hello World!";

	function show_greeting(){
		return $this->str;
	}
}

$message = new greeting;
//var_dump($message);
echo $message->show_greeting();

?>-->

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Example of PHP GET method</title>
</head>
<body>
	<?php
	if(isset($_REQUEST["name"])){
		echo "<p>Hi,".$_REQUEST["name"]."</p>";
	}


	?>
<form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
	<label for="inputName">Name:</label>
	<input type="text" name="name" id="inputName">
	<input type="submit" value="Submit">
	
</form>
</body>
</html>